import React from "react";
import "./Style.css";
import $ from 'jquery';
import {Link} from 'react-router-dom';
import menuBtn from "../assets/menu.svg"
import {HiOutlineMail} from 'react-icons/hi'


export default function Navigation() {

    $('body').css('padding-top', $('.navbar').outerHeight() + 'px')


    let prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        let currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else {
            document.getElementById("navbar").style.top = "-100px";
        }
        prevScrollpos = currentScrollPos;
    }



    return (
        <nav id="navbar" className="navbar navbar-expand-lg fixed-top">
            <div className="container pl-0 py-2 pt-3">
                <Link to="#page-top" className="navbar-brand text-white font-weight-bolder">
                    TECH YARD SOLUTIONS
                </Link>
                <button
                    type="button"
                    className="navbar-toggler"
                    data-toggle="collapse"
                    data-target="#navbar-1"
                >
                    <img src={menuBtn} alt=""/>
                </button>
                <div
                    className="collapse navbar-collapse"
                    id="navbar-1"
                >
                    <ul className="navbar-nav w-100 text-uppercase m-auto">
                        <li className="nav-item ml-4 mr-4">
                            <Link href="#about" className="nav-link text-white">
                                About us
                            </Link>
                        </li>
                        <li className="nav-item mr-4">
                            <Link href="#portfolio" className="nav-link text-white">
                                Portfolio
                            </Link>
                        </li>
                        <li className="nav-item mr-4">
                            <Link href="#services" className="nav-link text-white">
                                Services
                            </Link>
                        </li>
                        <li className="nav-item mr-4">
                            <Link to="#contact" className="nav-link text-white">
                                Contact
                            </Link>
                        </li>
                        <li className="nav-item ml-auto text-lowercase">
                            <Link to="https://www.google.com/" className="nav-link text-white">
                                <HiOutlineMail/> techyard@info.com
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}

